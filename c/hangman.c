//classic hangman attempt//
#include <stdio.h>
#include <string.h>
//int hangman; //array for hangman picture//
char userin[100]; //word to be guessed//
char reveal[100]; //array of revealed letters//
char guess[100]; //letter guessed//
int guesscount=0; //total number of guesses//
int wrongcount=0; //number of times incorrect guess made//
char picture[4][6]; //the hangingman progression//
int guesscheck; //checking for asterixes//

void hangman() 
{ //function drawing the  hangman//	
	if (wrongcount == 0){
		printf("\n \n \n \n");
	}
	if (wrongcount == 1){
		printf("\n|\n|\n|\n");
	}
	if (wrongcount == 2){
		printf("____\n|\n|\n|\n");
	}
	if (wrongcount == 3){
		printf("____\n| /\n|/\n|\n");
	}
	if (wrongcount == 4){
		printf("____\n| / \'\n|/\n|\n");
	}
	if (wrongcount == 5){
		printf("____\n| / O\n|/\n|\n");
	}
	if (wrongcount == 6){
		printf("____\n| / O\n|/  |\n|\n");
	}
	if (wrongcount == 7){
		printf("____\n| / O\n|/ /|\n|\n");
	}
	if (wrongcount == 8){
		printf("____\n| / O\n|/ /|\\ \n|\n");
	}
	if (wrongcount == 9){
		printf("____\n| / O\n|/ /|\\ \n|  /\n");
	}
	if (wrongcount == 10){
		printf("____\n| / O\n|/ /|\\ \n|  /\\ \n");
	}
	else{ 
		printf("\n \n \n \n");
	}
}

int main () {
printf("Enter word to be guessed: \n"); //start the game//
fgets(userin, sizeof(userin), stdin);
printf("length of word %d \n", strlen(userin));
int userpos;
for (userpos=0; userpos<(strlen(userin)-1); ++userpos){
	reveal[userpos]='*';
}
reveal[strlen(userin)-1]='\0';

printf("\n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n \n"); //clear the page//

printf("Guess the word before the hangman is drawn \n");

while (wrongcount < 10){
	guesscheck = 0;
	//guess=0;
	int position=0; //position in the userin//
	printf("Guess a letter: \n");
	fgets(guess,sizeof(guess),stdin);
	int size=sizeof(userin);
	// if the user input string is not empty
	if (guess[0]!='\n'){
		int correct=0;
		for (position=0; position<size; ++position){
			if (guess[0] == userin[position]){
				++correct;
				reveal[position]=guess[0];
			}
			if (reveal[position] == '*'){
				++guesscheck;
			}
		}
		if (correct==0){
			++wrongcount;
		}	
		++guesscount;	
		printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nTotal guesses made: %d  Total incorrect guesses made: %d/10 \n Revealed word: %s \n Your hangman: \n", guesscount, wrongcount, reveal);
                hangman();	
		if (guesscheck == 0){
			 printf("YOU WIN! YAS! \n\n");
		break;
		}
	}


}

if (wrongcount >= 10){
	printf("\n You dead, boy! \n\n");
}


return (0);
}
